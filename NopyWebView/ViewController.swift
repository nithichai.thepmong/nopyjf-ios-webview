//
//  ViewController.swift
//  NopyWebView
//
//  Created by Nithichai Thepmong on 8/5/2566 BE.
//

import UIKit
import WebKit

class ViewController: UIViewController {
    
    let webView: WKWebView = {
        let prefs = WKWebpagePreferences()
        prefs.allowsContentJavaScript = true

        let webConfiguration = WKWebViewConfiguration()
        
        let webView = WKWebView(frame: .zero, configuration: webConfiguration)
        
        return webView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(webView)
        
        guard let url = URL(string: "http://localhost:8000") else {
            return
        }
        
        webView.load(URLRequest(url: url))
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: {
//            self.webView.evaluateJavaScript("document.body.innerHTML") { result, error in
//                guard let html = result as? String, error == nil else {
//                    return
//                }
//
//                print(html)
//            }
//        })
    }


    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        webView.frame = view.bounds
    }
}

